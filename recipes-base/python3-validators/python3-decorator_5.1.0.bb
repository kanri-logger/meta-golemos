
SUMMARY = "Decorators for Humans"
HOMEPAGE = "https://github.com/micheles/decorator"
AUTHOR = "Michele Simionato <michele.simionato@gmail.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=be2fd2007972bf96c08af3293d728b22"

SRC_URI = "https://files.pythonhosted.org/packages/92/3c/34f8448b61809968052882b830f7d8d9a8e1c07048f70deb039ae599f73c/decorator-5.1.0.tar.gz"
SRC_URI[md5sum] = "d01585c3ea5b36a209747fcc978a98c8"
SRC_URI[sha256sum] = "e59913af105b9860aa2c8d3272d9de5a56a4e608db9a2f167a8480b323d529a7"

S = "${WORKDIR}/decorator-5.1.0"

RDEPENDS_${PN} = ""

inherit setuptools3
