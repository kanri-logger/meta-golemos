SUMMARY = "datalogger-setup-page recipe"
DESCRIPTION = "Recipe to add datalogger-setup-page to it's location."
LICENSE = "CLOSED"

inherit allarch perlnative

DEPENDS += " perl"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += " \
    git://git@gitlab.sclabs.io:10122/golemos/datalogger/setup-page.git;protocol=ssh; \
"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

DESTINATION = "/var/www/datalogger-setup.local"

do_install () {
    install -d ${D}${DESTINATION}
    cp -r ${S}/* ${D}${DESTINATION}
}

FILES_${PN} += "${DESTINATION}"

RDEPENDS_${PN} += " perl"
