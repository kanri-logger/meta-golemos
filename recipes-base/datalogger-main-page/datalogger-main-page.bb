SUMMARY = "datalogger-main-page recipe"
DESCRIPTION = "Recipe to add datalogger-main-page to it's location."
LICENSE = "CLOSED"

inherit allarch perlnative

DEPENDS += " perl"

SRC_URI += " git://git@gitlab.sclabs.io:10122/golemos/datalogger/main-page.git;protocol=ssh; "
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

DESTINATION = "/var/www/datalogger.local"

do_install () {
    install -d ${D}${DESTINATION}
    cp -r ${S}/* ${D}${DESTINATION}
}

FILES_${PN} += "${DESTINATION}"

RDEPENDS_${PN} += " perl"
