SUMMARY = "datalogger-api-request recipe"
DESCRIPTION = "Recipe to add basic datalogger functionality. Allow to use buttons and RFID scanner."
LICENSE = "CLOSED"

inherit allarch perlnative

DEPENDS += " perl"

SRC_URI += " git://git@gitlab.sclabs.io:10122/golemos/datalogger/api-request.git;protocol=ssh; "
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

DESTINATION = "/usr/bin"

do_install () {
    install -d ${D}${DESTINATION}
    cp -r ${S}/* ${D}${DESTINATION}
}

FILES_${PN} += "${DESTINATION}"

RDEPENDS_${PN} += " perl"

