
SUMMARY = "Bindings to the Linux input handling subsystem"
HOMEPAGE = "https://github.com/gvalkov/python-evdev"
AUTHOR = "Georgi Valkov <georgi.t.valkov@gmail.com>"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=18debddbb3f52c661a129724a883a8e2"

SRC_URI = "https://files.pythonhosted.org/packages/4d/ec/bb298d36ed67abd94293253e3e52bdf16732153b887bf08b8d6f269eacef/evdev-1.4.0.tar.gz"
SRC_URI[md5sum] = "919c1107b576771cfb0c43e2a8a4a405"
SRC_URI[sha256sum] = "8782740eb1a86b187334c07feb5127d3faa0b236e113206dfe3ae8f77fb1aaf1"

S = "${WORKDIR}/evdev-1.4.0"

RDEPENDS_${PN} = ""

inherit setuptools3
