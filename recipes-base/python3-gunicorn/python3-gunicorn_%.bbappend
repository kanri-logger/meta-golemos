inherit allarch systemd

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += " \
    file://datalogger_setup_page.service \
    file://datalogger_setup_page.socket \
"

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE__append = " datalogger_setup_page.service datalogger_setup_page.socket "

FILES_${PN} += "${systemd_system_unitdir}"
FILES_${PN} += "${systemd_system_unitdir}/datalogger_setup_page.service"
FILES_${PN} += "${systemd_system_unitdir}/datalogger_setup_page.socket"

do_install_append () {
    install -d ${D}/${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/datalogger_setup_page.s* ${D}/${systemd_system_unitdir}

    install -d ${D}${sysconfdir}/systemd/system/multi-user.target.wants/
    install -d ${D}${sysconfdir}/systemd/system/sockets.target.wants/

    ln -s ${systemd_system_unitdir}/datalogger_setup_page.socket ${D}${sysconfdir}/systemd/system/sockets.target.wants/datalogger_setup_page.socket
    ln -s ${systemd_system_unitdir}/datalogger_setup_page.service ${D}${sysconfdir}/systemd/system/multi-user.target.wants/datalogger_setup_page.service
}
