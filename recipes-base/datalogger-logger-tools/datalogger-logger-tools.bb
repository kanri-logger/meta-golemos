SUMMARY = "datalogger-logger-tools recipe"
DESCRIPTION = "Recipe to add logger-tools it's location."
LICENSE = "CLOSED"

inherit allarch perlnative

DEPENDS += " perl"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += " \
    git://git@gitlab.sclabs.io:10122/golemos/datalogger/logger-tools;protocol=ssh; \
"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

DESTINATION = "/opt/logger-tools"

do_install () {
    install -d ${D}${DESTINATION}
    cp -r ${S}/* ${D}${DESTINATION}
    ln -s ${D}${DESTINATION}/datalogger ${D}/bin/datalogger
    ln -s ${D}${DESTINATION}/app_handler.py ${D}/usr/lib/python3.9/app_handler.py
    cp ${D}${DESTINATION}/logger.yml ${D}/etc/logger.yml
    ln -s ${D}${DESTINATION}/datalogger-io.service ${D}/etc/systemd/system/datalogger-io.service
}

FILES_${PN} += "${DESTINATION}"

RDEPENDS_${PN} += " perl python3"
