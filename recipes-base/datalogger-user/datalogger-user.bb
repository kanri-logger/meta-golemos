SUMMARY = "datalogger-user recipe"
DESCRIPTION = "Recipe to add user for development purposes."
LICENSE = "CLOSED"

inherit useradd

# pw: abc
# usermod -p $(openssl passwd abc) root
# openssl passwd abc
# result: 1Cw5PHLy76ps2

# set image root password
EXTRA_USERS_PARAMS = "usermod -p 1Cw5PHLy76ps2 root;"

USERADD_PACKAGES = "${PN}"

# pw: xyz
# openssl passwd xyz
# result: y5UyLBO4GNAwc

USERADD_PARAM_${PN} = "-u 1200 -d /home/sclabs -r -s /bin/sh -p y5UyLBO4GNAwc sclabs"

do_install_append () {
    install -d -m 755 ${D}${datadir}/sclabs

    # The new users and groups are created before the do_install
    # step, so you are now free to make use of them:
    chown -R sclabs ${D}${datadir}/sclabs
}

FILES_${PN} = "${datadir}/*"
