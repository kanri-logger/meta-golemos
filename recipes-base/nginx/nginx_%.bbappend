FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += " \
    file://datalogger.local \
    file://datalogger-setup.local \
"

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE__append = " nginx.service "

do_install_append () {
    install -d ${D}${sysconfdir}/nginx/sites-available
    install -D -m 644 ${WORKDIR}/datalogger.local ${D}${sysconfdir}/nginx/sites-available/
    install -D -m 644 ${WORKDIR}/datalogger-setup.local ${D}${sysconfdir}/nginx/sites-available/

    rm -rf ${D}${sysconfdir}/nginx/sites-enabled/default_server
    ln -s ${sysconfdir}/nginx/sites-available/datalogger-setup.local ${D}${sysconfdir}/nginx/sites-enabled/
}
