DESCRIPTION = "Datalogger core application packagegroup"
SUMMARY = "Datalogger packagegroup - core system apps"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

RDEPENDS_${PN} = " \
    systemd \
    linux-firmware-rpidistro-bcm43430 \
    "
