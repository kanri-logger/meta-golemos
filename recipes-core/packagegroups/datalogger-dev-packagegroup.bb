DESCRIPTION = "Datalogger test application packagegroup"
SUMMARY = "Datalogger packagegroup - tools/testapps"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

RDEPENDS_${PN} = " \
    git \
    vim \
"
