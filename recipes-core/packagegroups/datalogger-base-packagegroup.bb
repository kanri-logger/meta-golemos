DESCRIPTION = "Datalogger base application packagegroup"
SUMMARY = "Datalogger packagegroup - base system apps"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

PACKAGES = " \
    datalogger-base-packagegroup \
    datalogger-base-buildtools \
    datalogger-base-python \
    datalogger-base-connectivity \
"

RDEPENDS_${PN} = " \
    datalogger-base-buildtools \
    datalogger-base-python \
    datalogger-base-connectivity \
    datalogger-user \
    datalogger-api-request \
    datalogger-main-page \
    datalogger-setup-page \
    datalogger-logger-tools \
"

SUMMARY_datalogger-base-buildtools = "Build utilities"
RDEPENDS_datalogger-base-buildtools = " \
    autoconf \
    automake \
    binutils \
    binutils-symlinks \
    cpp \
    cpp-symlinks \
    gcc \
    gcc-symlinks \
    g++ \
    g++-symlinks \
    gettext \
    make \
    libstdc++ \
    libtool \
    pkgconfig \
"

RDEPENDS_datalogger-base-buildtools-dev += " \
    libstdc++-dev \
"

SUMMARY_datalogger-base-python = "Python packages"
RDEPENDS_datalogger-base-python = " \
    python3 \
    libpython3 \
    python3-core \
    python3-flask \
    python3-gunicorn \
    python3-mfrc522 \
    python3-validators \
    python3-evdev \
    python3-rpi-ws281x \
    python3-pyyaml \
    python3-pip \
    python3-requests \
    python3-spidev \
    python3-wheel \
    python3-venv \
"

RDEPENDS_datalogger-base-python-dev += " \
    python3-dev \
"

SUMMARY_datalogger-base-connectivity = "Connectivity utilities"
RDEPENDS_datalogger-base-connectivity = " \
    curl \
    dropbear \
    hostapd \
    kea \
    nginx \
    rpi-gpio \
    rpio \
    wpa-supplicant \
"
