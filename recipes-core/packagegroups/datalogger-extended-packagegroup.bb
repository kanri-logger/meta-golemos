DESCRIPTION = "Datalogger extended application packagegroup"
SUMMARY = "Datalogger packagegroup - extended"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

RDEPENDS_${PN} = " \
    vim \
    git \
"
