SUMMARY = "Datalogger extended version of image"
LICENSE = "CLOSED"

inherit core-image
require datalogger-image.bb

CORE_IMAGE_EXTRA_INSTALL += " \
    datalogger-extended-packagegroup \
    "
