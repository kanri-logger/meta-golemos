SUMMARY = "Datalogger production image"
LICENSE = "CLOSED"

IMAGE_INSTALL += " packagegroup-core-boot"
IMAGE_FEATURES += " debug-tweaks"

inherit core-image

CORE_IMAGE_EXTRA_INSTALL += " \
    datalogger-core-packagegroup \
    datalogger-base-packagegroup \
    "
