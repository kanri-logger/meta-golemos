
### Shell environment set up for builds. ###

You can now run 'bitbake <target>'

Targets available from golemos layer:
    datalogger-image
    datalogger-extended-image
    datalogger-dev-image

Other commonly useful commands are:
 - 'devtool' and 'recipetool' handle common recipe tasks
 - 'bitbake-layers' handles common layer tasks
 - 'oe-pkgdata-util' handles common target package tasks
