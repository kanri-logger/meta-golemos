FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += " \
    file://wlan0.conf \
    file://hostapd@.service \
"

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE_${PN}_append = " hostapd@wlan0.service "

do_install_append () {
    install -d ${D}${sysconfdir}/hostapd
    install -D -m 600 ${WORKDIR}/wlan0.conf ${D}${sysconfdir}/hostapd/

    install -d ${D}${systemd_system_unitdir}
    install -D -m 0644 ${WORKDIR}/hostapd@.service ${D}${systemd_system_unitdir}/

    install -d ${D}${sysconfdir}/systemd/system/multi-user.target.wants/
    ln -s ${systemd_system_unitdir}/hostapd@.service ${D}${sysconfdir}/systemd/system/multi-user.target.wants/hostapd@wlan0.service
}
