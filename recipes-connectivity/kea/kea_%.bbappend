FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += " \
    file://kea-dhcp4.conf \
    file://kea-dhcp4.service \
"

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE_${PN}_append = " kea-dhcp4.service "

FILES_${PN} += " ${systemd_system_unitdir}/kea-dhcp4-server.service "

do_install_append () {
    install -d ${D}${sysconfdir}/kea/
    install -D -m 600 ${WORKDIR}/kea-dhcp4.conf ${D}${sysconfdir}/kea/

    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/kea-dhcp4.service ${D}${systemd_system_unitdir}

    install -d ${D}${sysconfdir}/systemd/system/multi-user.target.wants/
    ln -s ${systemd_system_unitdir}/kea-dhcp4.service ${D}${sysconfdir}/systemd/system/multi-user.target.wants/kea-dhcp4.service
}
